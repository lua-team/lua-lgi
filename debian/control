Source: lua-lgi
Section: interpreters
Priority: optional
Maintainer: Debian Lua Team <pkg-lua-devel@lists.alioth.debian.org>
Uploaders: Reiner Herrmann <reiner@reiner-h.de>
Build-Depends: dbus-x11,
               debhelper-compat (= 13),
               dh-lua,
               gir1.2-gtk-3.0,
               libcairo2-dev,
               libffi-dev,
               libgirepository1.0-dev,
               libglib2.0-dev,
               xauth,
               xvfb
Standards-Version: 4.6.0
Vcs-Git: https://salsa.debian.org/lua-team/lua-lgi.git
Vcs-Browser: https://salsa.debian.org/lua-team/lua-lgi
Homepage: https://github.com/lgi-devs/lgi
Rules-Requires-Root: no

Package: lua-lgi
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Provides: ${lua:Provides}
Depends: gir1.2-glib-2.0,
         ${misc:Depends},
         ${shlibs:Depends}
XB-Lua-Versions: ${lua:Versions}
Description: Lua bridge to GObject based libraries
 LGI is gobject-introspection based dynamic Lua binding to GObject based
 libraries. It allows using GObject-based libraries directly from Lua.
 .
 Notable examples are GTK+, GStreamer and Webkit.

Package: lua-lgi-dev
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Section: libdevel
Provides: ${lua:Provides}
Depends: lua-lgi (= ${binary:Version}),
         ${misc:Depends}
XB-Lua-Versions: ${lua:Versions}
Description: lgi development files for the Lua language
 This package contains the development files of the Lua lgi library,
 useful to create a statically linked binary (like a C application or a
 standalone Lua interpreter).
 Documentation is also shipped within this package.
